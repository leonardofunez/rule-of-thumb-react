import React, { useState } from 'react'
import data from '../../config/data'
import './MessageBox.scss'

const MessageBox = () => {
  const [open, setOpen] = useState(true)
  const close = () => {
    setOpen(!open)
  }

  return(
    <React.Fragment>
      {open ? 
        <div className='message-box bg-gray is-flex flex-between flex-align-center'>
          <div className='message-box__info is-flex flex-column'>
            <p className='message-box__subtitle'>{data[0].messageBox.subtitle}</p>
            <h2 className='message-box__title'>{data[0].messageBox.title}</h2>
          </div>
          <p className='message-box__description color-dark'>{data[0].messageBox.description}</p>
          <div className='message-box__close' onClick={() => close()}></div>
        </div>
      : 
        null
      }
    </React.Fragment>
  )
}

export default MessageBox