import React, { useState, useEffect } from 'react'
import './Post.scss'

const Post = (props) => {
  const [isLike, setLike] = useState(null)
  const [voteState, setVoteState] = useState(false)
  const [thanks, setThanks] = useState(false)
  const [likeAmount, setLikeAmount] = useState(0)
  const [dislikeAmount, setDislikeAmount] = useState(0)

  const postPhoto = require(`../../assets/img/posts/${props.photo}`)
  const postPhotoStyle = {
    background: `url(${postPhoto}) no-repeat center / cover`
  } 

  const selectVote = (state) => {
    setLike(state)
  }

  const sendVote = (post_id) => {
    let likedPosts = JSON.parse(localStorage.getItem('rotLikedPosts'))
    let dislikedPosts = JSON.parse(localStorage.getItem('rotDislikedPosts'))

    if( isLike !== null ){
      if( !isLike ){
        dislikedPosts = [...dislikedPosts, post_id]
        localStorage.removeItem('rotDislikedPosts')
        localStorage.setItem('rotDislikedPosts', JSON.stringify(dislikedPosts))
      }else{
        likedPosts = [...likedPosts, post_id] 
        localStorage.removeItem('rotLikedPosts')
        localStorage.setItem('rotLikedPosts', JSON.stringify(likedPosts))
      }

      setVoteState(true)
      setThanks(true)
      countVotes()
    }
  }

  const countVotes = () => {
    let likedPosts = JSON.parse(localStorage.getItem('rotLikedPosts'))
    let dislikedPosts = JSON.parse(localStorage.getItem('rotDislikedPosts'))
    
    let countLike = 0
    let countDislike = 0
    likedPosts.map( id => props.id === id ? countLike += 1 : null )
    dislikedPosts.map( id => props.id === id ? countDislike += 1 : null )
    
    if( countLike === 0 || countDislike === 0 ){
      if( countLike === 0 ) setDislikeAmount(100)
      if( countDislike === 0 ) setLikeAmount(100)
    }if(countLike === 0 && countDislike === 0){
      setLikeAmount(0)
      setDislikeAmount(0)
    }else{
      let totalVotes = countLike + countDislike
      let percLikes = (countLike / totalVotes) * 100
      let percDislikes = (countDislike / totalVotes) * 100
      
      setLikeAmount(Math.round(percLikes))
      setDislikeAmount(Math.round(percDislikes))
    }
  }

  const voteAgain = () => {
    setVoteState(false)
    setLike(null)
  }

  const isVoted = () => {
    setVoteState(
        true
      ? 
        (JSON.parse(localStorage.getItem('rotLikedPosts')).indexOf(props.id) !== -1) || 
        (JSON.parse(localStorage.getItem('rotDislikedPosts')).indexOf(props.id) !== -1)
      : 
        false
    )
  }

  const progressVotes = (type) => {
    if(type === 'like'){
      return `${likeAmount}%`
    }
    if(type === 'dislike'){
      return `${dislikeAmount}%`
    }
  }

  useEffect( () => {
    isVoted()
    countVotes()
  }, [])

  return(
    <article className='post is-flex flex-end color-white is-relative' style={postPhotoStyle}>
      { likeAmount !== dislikeAmount ?
        <div className={ likeAmount > dislikeAmount ? 'post__like-fixed like' : 'post__like-fixed dislike'}></div>
      :
        null
      }

      <div className='post__info'>
        <h2 className='post__title'>{props.title}</h2>
        <time className='post__date is-block'>
          <strong>{props.date}</strong> in {props.category}
        </time>
        <p className='post__description'>{!thanks ? props.description : 'Thank you for voting!'}</p>

        <div className='is-flex'>
          { !voteState ? 
            <React.Fragment>
              <div className='post__voting-button' data-state={ true ? isLike : null} onClick={() => selectVote(true)}>
                <div className='like bg-green w-100 h-100'></div>
              </div>
              <div className='post__voting-button' data-state={ true ? isLike === false : null} onClick={() => selectVote(false)}>
                <div className='dislike bg-orange w-100 h-100'></div>
              </div>
              
              <button className='button' onClick={() => sendVote(props.id)}>Vote now</button>
            </React.Fragment>
          :
            <button className='button' onClick={() => voteAgain()}>Vote again</button>
          }
        </div>
      </div>

      <div className='vote-results is-flex is-relative'>
        <div className='is-flex flex-between w-100 z-index-1'>
          <div className='vote-results__item is-flex flex-align-center'>
            <div className='like'></div>
            <div className='vote-results__amount'>{`${likeAmount}%`}</div>
          </div>
          <div className='vote-results__item is-flex flex-align-center'>
            <div className='vote-results__amount'>{`${dislikeAmount}%`}</div>
            <div className='dislike'></div>
          </div>
        </div>

        <div className='vote-results__progress is-flex flex-between w-100 h-100 is-absolute'>
          <div className='vote-results__progress-like' style={{width: progressVotes('like')}}></div>
          <div className='vote-results__progress-dislike' style={{width: progressVotes('dislike')}}></div>
        </div>
      </div>
    </article>
  )
}

export default Post