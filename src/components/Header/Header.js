import React, { useState, useEffect } from 'react'
import data from '../../config/data'
import { NavLink } from 'react-router-dom'
import SearchIcon from '../../assets/img/search.svg'
import './Header.scss'

const Header = () => {
  const [stateMenu, setStateMenu] = useState(false)
  
  const menu = () => {
    setStateMenu(!stateMenu)
  }

  const closeMenu = () => {
    setStateMenu(false)
  }

  useEffect( () => {
    if(localStorage.getItem('rotLikedPosts') === null){
      localStorage.setItem('rotLikedPosts', JSON.stringify([]))
    }
    if(localStorage.getItem('rotDislikedPosts') === null){
      localStorage.setItem('rotDislikedPosts', JSON.stringify([]))
    }
  })

  return(
    <header className='header is-fixed' data-state={ true ? stateMenu : false}>
      <div className='wrapper is-flex flex-between flex-align-center'>
        <div className='header__logo color-white'>
          <NavLink to={'/'} onClick={() => closeMenu()}>Rule of Thumb.</NavLink>
        </div>

        <div className='header__menu-button is-none is-absolute' data-state={ true ? stateMenu : false} onClick={() => menu()}>
          <div className='header__menu-line'></div>
          <div className='header__menu-line'></div>
        </div>

        <nav className='header__menu is-flex flex-align-center'>
          { data[0].main_menu.map( (item, index) => {
            return <NavLink key={index} to={`/${item.path}`} onClick={() => closeMenu()} className='header__menu-item'>{item.title}</NavLink>
          })}
          <div className='header__menu-item header__menu-auth-item color-white'>
            <NavLink to={'/log-in'} onClick={() => closeMenu()}>Log In</NavLink> <span>/</span> <NavLink to={'/sign-up'} onClick={() => closeMenu()}>Sign Up</NavLink>
          </div>
          <div className='header__menu-item search'>
            <img src={SearchIcon} width='25' alt='search' />
          </div>
        </nav>
      </div>
    </header>
  )
}

export default Header