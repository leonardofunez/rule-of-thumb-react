import React from 'react'
import data from '../../config/data'
import { NavLink } from 'react-router-dom'
import Facebook from '../../assets/img/facebook.svg'
import Twitter from '../../assets/img/twitter.svg'
import './Footer.scss'

const Footer = () => {
  return(
    <footer className='footer'>
      <div className='wrapper is-flex flex-align-center flex-between'>
        <div className='secondary-nav'>
          { data[0].secondary_menu.map( (item, index) => {
            return <NavLink key={index} to={item.path} className='secondary-nav__item'>{item.title}</NavLink>
          })}
        </div>

        <div className='social is-flex flex-align-center'>
          Follow Us
          <a className='social__item' href='/' target='_blank'>
            <img src={Facebook} width='20' alt='Facebook' />
          </a>
          <a className='social__item' href='/' target='_blank'>
            <img src={Twitter} width='23' alt='Twitter' />
          </a>
        </div>
      </div>
    </footer>
  )
}

export default Footer