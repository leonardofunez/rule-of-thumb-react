import React from 'react'
import './IntroPage.scss'
import PageBg from '../../assets/img/posts/page_bg.jpg'

const IntroPage = (props) => {
  const postBGStyle = {
    background: `url(${PageBg}) no-repeat center / cover`
  }

  return(
    <section className='intro-page is-relative bg-dark'>
      <div className='intro-page__bg is-absolute w-100 h-100' style={postBGStyle}></div>
      <div className='wrapper is-relative color-white'>
        <h1 className='intro-page__title'>{props.title}</h1>
        <p className='intro-page__description'>{props.description}</p>
      </div>
    </section>
  )
}

export default IntroPage