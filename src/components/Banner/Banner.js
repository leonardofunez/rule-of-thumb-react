import React from 'react'
import ImageBg from '../../assets/img/posts/page_bg.jpg'
import data from '../../config/data'
import './Banner.scss'

const Banner = () => {
  const postBGStyle = {
    background: `url(${ImageBg}) no-repeat center / cover`
  }

  return(
    <div className='banner bg-gray is-relative'>
      <div className='banner__bg is-absolute w-100 h-100' style={postBGStyle}></div>
      <div className='banner__info is-relative z-index-1 w-100 is-flex flex-between flex-align-center'>
        <p className='banner__description color-dark'>{data[0].banner.text}</p>
        <button className='button button__dark button__big'>{data[0].banner.button_text}</button>
      </div>
    </div>
  )
}

export default Banner