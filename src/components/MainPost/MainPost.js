import React from 'react'
import './MainPost.scss'

const MainPost = (props) => {
  const postPhoto = require(`../../assets/img/posts/${props.photo}`)
  const postPhotoStyle = {
    background: `url(${postPhoto}) no-repeat top / cover`
  }

  return(
    <article className='main-article is-relative' style={postPhotoStyle}>
      <div className='wrapper is-flex flex-align-center'>
        <div className='is-relative'>
          <div className='main-article__info is-relative color-white z-index-1'>
            <div className='main-article__top-text'>What's your opinion on</div>
            <h1 className='main-article__title'>{props.title}?</h1>
            <p className='main-article__description'>{props.description}</p>
            <a className='main-article__link' href={props.link} target='_blank' rel='noopener noreferrer'>More information</a>
            <div className='main-article__veredict'>
              <strong>What's Your Verdict?</strong>
            </div>

            <div className='voting-bar is-flex w-100 is-absolute'>
              <div className='voting-bar__button bg-green-op h-100 is-flex flex-justify-center'>
                <div className='like h-100 w-100'></div>
              </div>
              <div className='voting-bar__button bg-orange-op h-100 is-flex flex-justify-center'>
                <div className='dislike h-100 w-100'></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='main-article__closing-bar is-flex is-absolute'>
        <div className='main-article__closing'>CLOSING IN</div>
        <div className='main-article__date'><strong>22</strong> days</div>
      </div>
    </article>
  )
}

export default MainPost