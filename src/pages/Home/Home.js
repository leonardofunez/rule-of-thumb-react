import React from 'react'

import data from '../../config/data'
import Post from '../../components/Post/Post'
import MainPost from '../../components/MainPost/MainPost'
import MessageBox from '../../components/MessageBox/MessageBox'
import Banner from '../../components/Banner/Banner'

const Home = () => {
  const mainPostData = data[0].posts[0]
  return(
    <section className='page page__home'>
      <MainPost
        title={mainPostData.title}
        description={mainPostData.description}
        photo={mainPostData.photo}
        date={mainPostData.date}
        link={mainPostData.link}
      />

      <div className='wrapper'>
        <MessageBox />

        {/* Post List */}
          <section className='post-list'>
            <h2 className='block-title'>Votes</h2>
            <div className='is-flex flex-wrap'>
              { data[0].posts.map((post, index) => {
              if(index !== 0) {
                  return <Post
                          key={index}
                          id={post.id}
                          type={index === 0 ? 'main' : 'normal'}
                          title={post.title}
                          description={post.description}
                          photo={post.photo}
                          date={post.date}
                          category={post.category}
                        />
                }
              })}
            </div>
          </section>
        {/* .Post List */}

        <Banner />
      </div>
    </section>
  )
}

export default Home