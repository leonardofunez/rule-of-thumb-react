import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const PrivacyPolicy = () => {
  return(
    <section className='page page__privacy-policy'>
      <IntroPage title={data[0].pages[0].privacy_policy.title} description={data[0].pages[0].privacy_policy.description} />
    </section>
  )
}

export default PrivacyPolicy