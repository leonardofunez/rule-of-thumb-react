import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const SignUp = () => {
  return(
    <section className='page page__sign-up'>
      <IntroPage title={data[0].pages[0].sign_up.title} description={data[0].pages[0].sign_up.description} />
    </section>
  )
}

export default SignUp