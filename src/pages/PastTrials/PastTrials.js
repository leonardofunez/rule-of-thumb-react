import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const PastTrials = () => {
  return(
    <section className='page page__past-trials'>
      <IntroPage title={data[0].pages[0].past_trial.title} description={data[0].pages[0].past_trial.description} />
    </section>
  )
}

export default PastTrials