import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const LogIn = () => {
  return(
    <section className='page page__log-in'>
      <IntroPage title={data[0].pages[0].login.title} description={data[0].pages[0].login.description} />
    </section>
  )
}

export default LogIn