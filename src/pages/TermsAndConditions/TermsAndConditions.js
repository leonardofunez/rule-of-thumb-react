import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const TermsAndConditions = () => {
  return(
    <section className='page page__terms-and-conditions'>
      <IntroPage title={data[0].pages[0].terms_and_conditions.title} description={data[0].pages[0].terms_and_conditions.description} />
    </section>
  )
}

export default TermsAndConditions