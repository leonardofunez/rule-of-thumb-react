import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const ContactUs = () => {
  return(
    <section className='page page__contact-us'>
      <IntroPage title={data[0].pages[0].contact_us.title} description={data[0].pages[0].contact_us.description} />
    </section>
  )
}

export default ContactUs