import React from 'react'
import IntroPage from '../../components/IntroPage/IntroPage'
import data from '../../config/data'

const HowItWorks = () => {
  return(
    <section className='page page__how-it-works'>
      <IntroPage title={data[0].pages[0].how_it_works.title} description={data[0].pages[0].how_it_works.description} />
    </section>
  )
}

export default HowItWorks