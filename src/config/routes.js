import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'

// Pages
import Home from '../pages/Home/Home'
import PastTrials from '../pages/PastTrials/PastTrials'
import HowItWorks from '../pages/HowItWorks/HowItWorks'
import TermsAndConditions from '../pages/TermsAndConditions/TermsAndConditions'
import PrivacyPolicy from '../pages/PrivacyPolicy/PrivacyPolicy'
import ContactUs from '../pages/ContactUs/ContactUs'
import LogIn from '../pages/LogIn/LogIn'
import SignUp from '../pages/SignUp/SignUp'

const routes = (
	<Router>
		<React.Fragment>
      <main className='rot-main'>
        <Route path='/' component={Header} />

        <Route path='/' component={Home} exact/>
        <Route path='/past-trials' component={PastTrials} exact/>
        <Route path='/how-it-works' component={HowItWorks} exact/>
        <Route path='/terms-and-conditions' component={TermsAndConditions} exact/>
        <Route path='/privacy-policy' component={PrivacyPolicy} exact/>
        <Route path='/contact-us' component={ContactUs} exact/>
        <Route path='/log-in' component={LogIn} exact/>
        <Route path='/sign-up' component={SignUp} exact/>

        <Route path='/' component={Footer} />
      </main>
    </React.Fragment>
  </Router>
);

export default routes